<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230612141847 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Миграция для таблицы модели';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `model` (`id` int NOT NULL AUTO_INCREMENT, `name` text NOT NULL, `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `isActive` boolean DEFAULT false,  PRIMARY KEY (`id`), INDEX(`isActive`))');
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DROP TABLE IF EXISTS model");
        // this down() migration is auto-generated, please modify it to your needs

    }
}
