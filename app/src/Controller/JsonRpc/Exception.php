<?php

namespace App\Controller\JsonRpc;

/**
 * Исключения для JSON RPC
 */
class Exception extends \Exception
{
    /** @var int Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text */
    public const PARSE_ERROR = -32700;

    /** @var int The JSON sent is not a valid Request object */
    public const INVALID_REQUEST = -32600;

    /** @var  int The method does not exist / is not available */
    public const METHOD_NOT_FOUND = -32601;

    /** @var int Invalid method parameter(s) */
    public const INVALID_PARAMS = -32602;

    /** @var int Internal JSON-RPC error. */
    public const INTERNAL_ERROR = -32603;
}