<?php

namespace App\Controller;

use App\Controller\JsonRpc\JsonRpc;
use App\DTO\ModelDTO;
use App\Service\DataService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DataController extends JsonRpc
{

    /**
     * Конструктор
     */
    public function __construct(
        private readonly DataService $service
    )
    {
    }

    #[Route('/model', name: 'model')]
    public function index(Request $request): JsonResponse
    {
        return parent::index($request);
    }

    public function create(ModelDTO $params): array
    {
        return $this->service->create($params);
    }

    public function update(ModelDTO $params): array
    {
        return $this->service->update($params);
    }

    public function delete(ModelDTO $params): array
    {
        return $this->service->delete($params);
    }
}