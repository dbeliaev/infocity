<?php

namespace App\Service;

use App\Controller\JsonRpc\Exception;
use App\DTO\ModelDTO;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\DBAL\Connection;
use PDO;

class DataService
{
    /** @var ?Connection $db БД */
    protected ?Connection $db = null;

    const LIMIT = 500;

    /**
     * Конструктор
     *
     * @param ManagerRegistry $doctrine
     */
    public function __construct(
        private readonly ManagerRegistry $doctrine
    )
    {
    }

    /**
     * Создание модели
     *
     * @param ModelDTO $params
     * @return array
     * @throws Exception
     */
    public function create(ModelDTO $params): array
    {
        //TODO: для создания записи сделать массив чтобы можно было добавить сразу несколько записей (если такая возможность вообще нужна)
        if ($params->name === null) {
            throw new Exception("Param id must be set", Exception::INVALID_PARAMS);
        }

        $sql = "INSERT INTO infocity.model (name, isActive) VALUES (:name, :isActive)";
        try {
            $query = $this->getDb()->prepare($sql);
            $query->bindValue("name", $params->name, PDO::PARAM_STR);
            $query->bindValue("isActive", $params->isActive, PDO::PARAM_BOOL);
            $query->executeQuery();
        } catch (\Throwable $e) {
            throw new Exception("Can`t connect to db", Exception::INTERNAL_ERROR);
        }

        //TODO: Лушче использовать rabbit, чтобы клиент не ждал пока мы обработаем 100000+ строк .... + возможность распараллелить выборку.
        if ($params->isActive) {
            $this->genFile();
        }

        return [
            'success' => 'ok'
        ];
    }

    /**
     * Обновление модели
     *
     * @param ModelDTO $params
     * @return array
     * @throws Exception
     */
    public function update(ModelDTO $params): array
    {
        if ($params->id === null || $params->name === null) {
            throw new Exception("Param id must be set", Exception::INVALID_PARAMS);
        }
        $sql = "UPDATE infocity.model SET name = :name, isActive = :isActive WHERE id = :id";
        try {
            $query = $this->getDb()->prepare($sql);
            $query->bindValue("id", $params->id, PDO::PARAM_INT);
            $query->bindValue("name", $params->name, PDO::PARAM_STR);
            $query->bindValue("isActive", $params->isActive, PDO::PARAM_BOOL);
            $query->executeQuery();
        } catch (\Throwable $e) {
            throw new Exception("Can`t connect to db", Exception::INTERNAL_ERROR);
        }

        //TODO: Лушче использовать rabbit, чтобы клиент не ждал пока мы обработаем 100000+ строк .... + возможность распараллелить выборку.

        $this->genFile();

        return [
            'success' => 'ok'
        ];
    }

    /**
     * Удаление записи
     *
     * @param ModelDTO $params
     * @return array
     * @throws Exception
     */
    public function delete(ModelDTO $params): array
    {
        if ($params->id === null) {
            throw new Exception("Param id must be set", Exception::INVALID_PARAMS);
        }

        $sql = "DELETE FROM infocity.model WHERE id = :id";
        try {
            $query = $this->getDb()->prepare($sql);
            $query->bindValue("id", $params->id, PDO::PARAM_INT);
            $result = $query->executeQuery()->fetchAllAssociative();
        } catch (\Throwable $e) {
            throw new Exception("Can`t connect to db", Exception::INTERNAL_ERROR);
        }

        //TODO: Лушче использовать rabbit, чтобы клиент не ждал пока мы обработаем 100000+ строк .... + возможность распараллелить выборку.
        //TODO: Если иcпользовать  Entity -> то можно опимизировать работу чтобы каждый раз не лезть в файл, но сама по себе entity медленно работает.
        //TODO: В данном случае если у нас только один запрос entity +- оправдана. (Но тк одновременно может быть множество запросов я не стал её испльзовать)
        $this->genFile();

        return [
            "id" => $result['id']
        ];
    }

    /**
     * Подключение к бд
     *
     * @return Connection
     */
    protected function getDb(): Connection
    {
        if ($this->db === null) {
            $this->db = $this->doctrine->getConnection();
        }

        return $this->db;
    }

    /**
     *
     * Гененрируем файл
     *
     * @return void
     * @throws Exception
     */
    public function genFile()
    {
        //TODO: я бы добавил кеш чтобы не гнать всю базу из-за одной записи
        //TODO: Лучше задать через параметры в symfony
        $fileName  = '/var/www/file/model.txt';
        $fp = fopen($fileName, 'w+');
        foreach ($this->getDataToFile() as $row) {
            foreach ($row as $item) {
                $item = array_values($item);
                if (!empty($item)) fwrite($fp, implode(",", $item) . PHP_EOL);
            }
        }
        fclose($fp);
    }


    /**
     * Достаём данные для файла
     *
     * @return \Generator
     * @throws Exception
     */
    protected function getDataToFile(): \Generator
    {
        $i = 0;
        do{
            $sql = "SELECT id, name, updated_at FROM infocity.model WHERE isActive = true and id > :id LIMIT :limit";
            try {
                $query = $this->getDb()->prepare($sql);
                $query->bindValue("id", $i, PDO::PARAM_INT);
                $query->bindValue("limit", self::LIMIT, PDO::PARAM_INT);
                $result = $query->executeQuery()->fetchAllAssociative();
                yield $result;
            } catch (\Throwable $e) {
                throw new Exception("Can`t connect to db", Exception::INTERNAL_ERROR);
            }

            $i +=self::LIMIT;

        }while($result !== []);
    }
}