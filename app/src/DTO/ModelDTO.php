<?php

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class ModelDTO extends DataTransferObject
{
    /** @var ?int $id Id записи */
    public ?int $id = null;

    /** @var string|null $name Имя для модели */
    public ?string $name = null;

    /** @var bool $isActive Статутс записи */
    public bool $isActive = false;
}