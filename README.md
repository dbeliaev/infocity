
### Конфигурация

#### Докер 
> docker-compose up -d --build

1. #### Создать бд

> CREATE DATABASE infocity char set utf8mb4;

2. #### Composer

> composer update 
2. #### Запуск миграции

> php bin/console doctrine:migrations:migrate "DoctrineMigrations\Version20230612141847"

3. #### В данном случае для записи в файл в нутри докера  


> chmod -R 777 /var/www
***
> Запросы отправляются методом POST на адрес /model
### Методы
1. #### Создать новую запись. 
> method: create
### Входные параметры

| имя        | тип    | обяз. | описание                                         |    
|------------|--------|-------|--------------------------------------------------|
| name       | string | О     | Имя Модели                                       |
| isActive   | bool   | Н     | Является ли запись активной (по умодчанию false) |



Пример запроса:
```php
{
  "jsonrpc": "2.0",
   "method": "create",
   "id": 1,
   "params": {
     "name": "test",
     "isActive": true
   }
}
```
Пример ответа:
```php
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "success": "ok"
  }
}

```

2. #### Обновить модель
> method: update
### Входные параметры

| имя      | тип    | обяз. | описание                                         |    
|----------|--------|-------|--------------------------------------------------|
| id       | int    | О     | id из БД                                         |
| name     | string | О     | Имя Модели                                       |
| isActive | bool   | Н     | Является ли запись активной (по умодчанию false) |


Пример запроса:
```php
{
  "jsonrpc": "2.0",
   "method": "update",
   "id": 1,
   "params": {
     "id": 29,
     "name": "test1",
     "isActive": true
   }
}
```
Пример ответа:
```php
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "success": "ok"
  }
}

```

3. #### Обновить модель
> method: delete
### Входные параметры

| имя      | тип    | обяз. | описание                                         |    
|----------|--------|-------|--------------------------------------------------|
| id       | int    | О     | id из бд                                         |
| name     | string | О     | Имя Модели                                       |
| isActive | bool   | Н     | Является ли запись активной (по умодчанию false) |


Пример запроса:
```php
{
  "jsonrpc": "2.0",
   "method": "delete",
   "id": 1,
   "params": {
     "id": 28,
   }
}
```
Пример ответа:
```php
{
  "jsonrpc": "2.0",
  "id": 1,
  "result": {
    "success": "ok"
  }
}

```